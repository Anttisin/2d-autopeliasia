﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CarMovement : MonoBehaviour 
{ 
	public HingeJoint2D rearWheel, frontWheel; 
	public float speed = 150f;
	public float motorSpeed = 1000f;
	public float rotationForce = 40f;

	public Text speedMeter;

	JointMotor2D jointMotor; 


	private Rigidbody2D rb;

	void Start () {	
		rb = GetComponent<Rigidbody2D> ();
	} 

	// Update is called once per frame 
	void Update () {
		speedMeter.text = (rb.velocity.magnitude * 3.6f).ToString ("000");
		if (Input.GetKey (KeyCode.UpArrow)) { 
			jointMotor.maxMotorTorque = speed;
			jointMotor.motorSpeed = 1000f;
		} else if (Input.GetKey(KeyCode.DownArrow)) { 
			jointMotor.maxMotorTorque = speed * 0.3f;
			jointMotor.motorSpeed = -300f;
		} else {
			jointMotor.maxMotorTorque = 0; 
		}
		frontWheel.motor = jointMotor;
		rearWheel.motor = jointMotor;

		if (Input.GetKey (KeyCode.LeftArrow)) {
			rb.AddTorque(rotationForce);
		} else if (Input.GetKey (KeyCode.RightArrow)) {
			rb.AddTorque(-rotationForce);
		}
	} 
}