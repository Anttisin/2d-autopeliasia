﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class TimeDisplay : MonoBehaviour {

	public float secondsLeft = 60; // Time available at start of level.
    private string nicetime = "0:00";
	Text UITimeText; 

	// Use this for initialization
	void Start () {
		UITimeText = GetComponent<Text> ();
        UITimeText.text = nicetime;

		InvokeRepeating ("UpdateTime", 0, 1.0f); // Update time display every second.
	
	}
	
	// Update is called once per frame
	void Update () {

		// Are we out of time? Restart level!
		if (secondsLeft <= 0) {
			GameManager.instance.RestartLevel(); // Restart scene.
		}

        this.UpdateDisplayTime();


	}

	void UpdateTime() {
	
		secondsLeft--; // One second has passed.
	
	}

    void UpdateDisplayTime()
    {
        // Fancy stuff to format time nicely.
        int minutes = Mathf.FloorToInt(secondsLeft / 60F);
        int seconds = Mathf.FloorToInt(secondsLeft - minutes * 60);
        nicetime = string.Format("{0:0}:{1:00}", minutes, seconds);
        UITimeText.text = nicetime;
    }

    public void AddSeconds(float seconds)
    {
        Debug.Log("Adding seconds!");
        secondsLeft = secondsLeft + seconds;
    }

}
