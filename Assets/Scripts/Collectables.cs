﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Collectables : MonoBehaviour
{
    public GameObject teleportOut;
    public float turboKerroin;
    private GameObject timerDisplay;
    public float timeCrystalBenefit = 1.1f;
    public AudioSource crystalSound;
    public AudioSource teleportSound;
    public AudioSource turboSound;



    private Vector2 telep;
    private int count;
    private Rigidbody2D rb;


    void Start()
    {

        timerDisplay = GameObject.FindGameObjectWithTag("Timer");

        count = 0;
        if (teleportOut)
            telep = teleportOut.GetComponent<Transform>().position;

        rb = GetComponent<Rigidbody2D>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (teleportOut && other.gameObject.CompareTag("Teleportti")) // Teleportti
        {
            if (teleportSound)
            {
                teleportSound.Play();
            }
            transform.position = telep;
            Destroy(other.gameObject);
            Destroy(teleportOut.gameObject);
        }
        if (other.gameObject.CompareTag("Pickup"))  // Pisteet

        {
            if (crystalSound)
            {
                crystalSound.Play();
            }
            Destroy(other.gameObject);
            count = count + 1;
            if (timerDisplay != null)
            {
                Debug.Log("Add seconds");
                timerDisplay.GetComponent<TimeDisplay>().AddSeconds(timeCrystalBenefit);
            }
                Debug.Log("Score: " + count);
        }
        if (other.gameObject.CompareTag("Turbo"))   // Turboboost
        {
            if (turboSound)
            {
                turboSound.Play();
            }
            rb.AddForce(new Vector2(100 * turboKerroin, 600), ForceMode2D.Impulse);
            Destroy(other.gameObject);
            

        }
    }

}