﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MusicVolume : MonoBehaviour {

	public Slider musicSlider;
	public AudioSource music;


	public void OnValueChanged ()
	{
		music.volume = musicSlider.value;

	}
}