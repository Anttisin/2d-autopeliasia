﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

    float speed; //speed
    Vector2 _direction; //direction of ze bullet
    bool isReady; // to know when bullet direction is set

    void Awake()
    {
        speed = 5f;
        isReady = false;
    }

	// Use this for initialization
	void Start () {
		
	}
	// bullet's direction
    public void SetDirection(Vector2 direction)
    {
        // direction normalized, to get unit vector
        _direction = direction.normalized;

        isReady = true;

    }


	// Update is called once per frame
	void Update ()
    {
		if(isReady)
        {
            // bullet's current position
            Vector2 position = transform.position;
            
            // compute new position
            position += _direction * speed * Time.deltaTime;

            // update the position
            transform.position = position;

            // destroy the bullet if conditions apply

            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

            Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

            if ((transform.position.x < min.x) || (transform.position.x > max.x) || 
                    (transform.position.y < min.y) || (transform.position.y > max.y))
                {
                Destroy(gameObject);
                }
        }
	}
}
