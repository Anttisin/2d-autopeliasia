﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        Vector3 newPosition = target.position;
        //Zoom out the camera on Z axis to see something
        newPosition.z = -10;

        transform.position = newPosition;
    }
}
