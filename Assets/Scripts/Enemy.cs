﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public GameObject bloodSpatter;
    private GameObject blood;

    private bool deleted = false;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!deleted && collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Touching The Player");
                if (collision.gameObject.GetComponent<CarController>().IsWheeling()
                //&& gameObject.GetComponent<BoxCollider2D>().IsTouching(collision.gameObject.GetComponent<CarController>().wheelieCollider) 
                )
            {
                deleted = true;
                Debug.Log("Kill enemy");
                GameManager.instance.AddKillCount();
                this.SpatterBlood();
                gameObject.GetComponent<AudioSource>().Play();
                
                Invoke("deleteObject", 0.3f);
            } else
            {
                Debug.Log("Restart by enemy");
                GameManager.instance.RestartLevel();
            }
        }
    }

    private void SpatterBlood()
    {
        Debug.Log("BLOOD");
        blood = Instantiate(bloodSpatter, transform.position, transform.rotation);
    }

    private void deleteObject()
    {
        Destroy(gameObject);
  
    }
    
}
