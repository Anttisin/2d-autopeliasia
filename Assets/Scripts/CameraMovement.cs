﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	public GameObject player;
	public float maxZoom;
	public float zoomKerroin;
	private Vector3 offset;
	private float defaultCamera;

	void Start ()
	{
		offset = transform.position - player.transform.position;
		defaultCamera = GetComponent<Camera> ().orthographicSize;
	}

	void LateUpdate ()
	{
		transform.position = player.transform.position + offset;

		if (player.GetComponent<Rigidbody2D> ().velocity.magnitude > 1)
		
		{
			Camera.main.orthographicSize = defaultCamera + zoomKerroin * player.GetComponent<Rigidbody2D> ().velocity.magnitude;

			if (Camera.main.orthographicSize >= maxZoom)
			{
				Camera.main.orthographicSize = maxZoom;
			}
		}


	}


}
	
	