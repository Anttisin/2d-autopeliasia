﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuLoader : MonoBehaviour {

	public GameObject soundManager;

	// Use this for initialization
	void Awake () {
		
		if (SoundManager.instance == null)
		{
			Instantiate(soundManager);
		}
	}

	// Update is called once per frame
	void Update () {

	}
}

