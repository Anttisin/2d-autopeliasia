﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaKillExplosion : MonoBehaviour
{

    public GameObject plasmaExplosion;
    private GameObject explosion;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("PLASMAHIT");
            PlasmaExplosion();
            Invoke("triggerRestart", 0.5f);
        }
    }

    private void PlasmaExplosion()
    {
        Debug.Log("PLASMA EXPlOSION");
        explosion = Instantiate(plasmaExplosion, transform.position, transform.rotation);        
    }

    private void triggerRestart()
    {
        GameManager.instance.RestartLevel();
    }

}
