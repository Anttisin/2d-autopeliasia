﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Space_scroll1 : MonoBehaviour {

	void Update () {

        MeshRenderer mr = GetComponent<MeshRenderer>();

        Material mat = mr.material;

        Vector2 offset = mat.mainTextureOffset;

        offset.x += Time.deltaTime / 40f;

        mat.mainTextureOffset = offset;

		
	}
}
