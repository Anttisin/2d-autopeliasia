﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MasterVolume : MonoBehaviour {

	public Slider masterSlider;

	public void OnValueChanged ()
	{
		AudioListener.volume = masterSlider.value;
	}
}
