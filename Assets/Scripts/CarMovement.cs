﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CarMovement : MonoBehaviour 
{ 
	public HingeJoint2D rearWheel, frontWheel; 
	public float speed = 150f;
	public Text speedMeter;
	JointMotor2D jointMotor; 

	private Quaternion cameraRotation;

	void Start () {	} 

	// Update is called once per frame 
	void Update () {
		speedMeter.text = (GetComponent<Rigidbody2D> ().velocity.magnitude * 3.6f).ToString ("000");
		if (Input.GetKey (KeyCode.Space)) { 
			jointMotor.maxMotorTorque = speed;
		} else {
			jointMotor.maxMotorTorque = 0; 
		}
		jointMotor.motorSpeed = 1000;
		frontWheel.motor = jointMotor;
		rearWheel.motor = jointMotor;

	} 
}