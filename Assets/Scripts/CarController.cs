﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{

    public float maxSpeed = 200f;
    public float rotationSpeed = 15f;

    public float motorPower = 5000f;
    public float decelerationSpeed = 100f;
    public float jumpForce = 700;

    public WheelJoint2D rearWheel;
    public WheelJoint2D frontWheel;
    public Transform centerOfMass;

    public Rigidbody2D rb;
    public BoxCollider2D wheelieCollider;

    public AudioClip jumpSound;
    public AudioSource engineSound;

    JointMotor2D wheelMotor;

    private float currentSpeed;
    private float currentVelocity;
    private float currentRotation;
    private float movement = 0f;
    private float brakeSpeed;
    private bool wheelie = false;

    private bool grounded = false;
    private bool doJump = false;
    private bool doWheelie = false;
    public Transform groundCheck;
    public float groundRadius = 0.2f;
    public LayerMask whatIsGround;



    // Use this for initialization
    void Start()
    {
        wheelMotor = rearWheel.motor;
        rb.centerOfMass = centerOfMass.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        movement = Input.GetAxisRaw("Horizontal");
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);


        if (Input.GetKeyDown(KeyCode.Space))
        {
            doJump = true;
        }

        if (Input.GetKeyDown(KeyCode.W) && currentVelocity > 8)
        {
            wheelie = true;
        }

        if (Input.GetKeyUp(KeyCode.W) || currentVelocity < 8)
        {
            wheelie = false;
        }


        this.HandleEngineSoundPitch();

        if (engineSound && movement != 0)
        {
            if (!engineSound.isPlaying)
            {
                engineSound.Play();
            }

        } else if (engineSound && movement == 0 && engineSound.isPlaying)
        {
            engineSound.Stop();
        }

    }

    void FixedUpdate()
    {

        if (doJump)
        {
            this.Jump();
        }

        if (wheelie)
        {
            this.DoAWheelie();
        }

        if (!wheelie)
        {
            this.StopWheelie();
        }


        this.BalanceInAir();
        Debug.Log("CUrrent velocity " + currentVelocity);
        Debug.Log("Max speed: " + maxSpeed);
        //Debug.Log(currentRotation);



            if (currentRotation > 0.25)
            {
                rb.MoveRotation(rb.rotation  - 10f);
            }
            else if (currentRotation < -0.25)
            {
                rb.MoveRotation(rb.rotation + 10f);
            }

            if (wheelie)
        {
            rb.MoveRotation(35);
        }

        //Debug.Log("Motor Speed: " + wheelMotor.motorSpeed);

        if (currentSpeed > maxSpeed)
        {
            motorPower = 0;
        }
        else
        {
            motorPower = 5000f;
        }

        if (movement == 0f)
        {
            wheelMotor.motorSpeed = Mathf.Lerp(wheelMotor.motorSpeed, 0, decelerationSpeed);

        }
        else if (movement > 0)
        {
            if (currentVelocity < 0 && grounded)
            {
                brakeSpeed = 1f;
                rb.AddForce(new Vector2(500,0), ForceMode2D.Impulse);
                wheelMotor.motorSpeed = Mathf.Lerp(wheelMotor.motorSpeed, 0, brakeSpeed);
            } else
            {
                brakeSpeed = 1000f;
                wheelMotor.motorSpeed = Mathf.Lerp(wheelMotor.motorSpeed, -motorPower, brakeSpeed);
            }

        }
        else if (movement < 0)
        {
            if (currentVelocity > 0 && grounded)
            {
                brakeSpeed = 1f;
                Debug.Log("Braking.");
                rb.AddForce(new Vector2(-500, 0), ForceMode2D.Impulse);
                wheelMotor.motorSpeed = Mathf.Lerp(wheelMotor.motorSpeed, 0, brakeSpeed);
            }
            else
            {
                brakeSpeed = 1000f;
                wheelMotor.motorSpeed = Mathf.Lerp(wheelMotor.motorSpeed, motorPower, brakeSpeed);
            }

        }

       

        rearWheel.motor = wheelMotor;


    }

    void LateUpdate()
    {
        currentVelocity = rb.velocity.x;
        currentSpeed = rb.velocity.magnitude;
        if (movement < 0)
        {
            currentSpeed = -currentSpeed;
        }

        currentRotation = transform.rotation.z;
    }

    private void HandleEngineSoundPitch()
    {
        if (engineSound && engineSound.isPlaying)
        {
            engineSound.pitch = currentVelocity / 30;
        }
    }

    private void Jump()
    {
        Debug.Log("Jump");
        Debug.Log("Grounded: " + grounded);
        if (grounded)
        {
            if (jumpSound)
        {
            SoundManager.instance.PlaySingle(jumpSound);
        }

            rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
        }

        doJump = false;
    }

    private void DoAWheelie()
    {
            Debug.Log("Do a wheelie");
            wheelie = true;
    }

    private void StopWheelie()
    {
        wheelie = false;
    }

    private void BalanceInAir()
    {
        if (!grounded && movement != 0)
        {
            //Debug.Log("Rotation: " + rb.rotation);
            if (currentRotation > 0.25)
            {
                rb.MoveRotation((rb.rotation - 5 * -movement) + 10000f);
            }
            else if (currentRotation < -0.25)
            {
                rb.MoveRotation((rb.rotation + 5 *  -movement) + 10000f);
            }

        }
    }

    public bool IsWheeling()
    {
        return wheelie;
    }


}
