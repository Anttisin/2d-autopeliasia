﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    public LevelManager levelScript;

    private int startLevel = 0;
    private int level = 0;

    private int killCount = 0;

	// Use this for initialization



	void Awake () {
        if (instance == null)
        {
            instance = this;
        } else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        levelScript = GetComponent<LevelManager>();
        InitGame();
	}

    void InitGame()
    {
        levelScript.LoadScene(startLevel);
    }

    public void RestartLevel()
    {
        levelScript.LoadScene(level);
    }

    public void LoadNextLevel()
    {
        level = level + 1;
        levelScript.LoadScene(level);
    }
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButton ("Cancel"))
		{
			SceneManager.LoadScene("MainMenu");
		}
	}

    public void AddKillCount()
    {
        killCount++;
    }

    public int GetKillCount()
    {
        return killCount;
    }


    void OnTriggerEnter2D()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
