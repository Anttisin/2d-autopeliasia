﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGun : MonoBehaviour {

    public GameObject EnemyBullet; //enemybullet prefab
    public float interval;
    public AudioSource firingSound;

    
	// Use this for initialization
	void Start () {

        // fire after 1 second
        // Invoke ("FireEnemyBullet", 1f);
        // Repeated :
        InvokeRepeating("FireEnemyBullet", 2.0f, interval);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    // fire enemy bullet
    void FireEnemyBullet()
    {
        // define player
        GameObject playerShip = GameObject.Find("PlayerCar");

        // player not dead
        if (playerShip != null)
        {
            if (firingSound)
            {
                firingSound.Play();
            }

            GameObject bullet = (GameObject)Instantiate(EnemyBullet);

            //set bullets position
            bullet.transform.position = transform.position;

            // compute direction
            Vector2 direction = playerShip.transform.position - bullet.transform.position;

            //set the direction
            bullet.GetComponent<EnemyBullet>().SetDirection(direction);


        }

    }


}
