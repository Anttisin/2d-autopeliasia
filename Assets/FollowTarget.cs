﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{

    public Transform target;
    public float speed = 3f;
    public float followDistance = 80f;
    public bool stopAtMinimumDistance = false;
    public float mininumDistance = 1f;
    public bool keepGround = false;

    public Rigidbody2D rb;

    private GameObject blood;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();

        if (!target)
        {
            target = GameObject.FindWithTag("Player").transform;
        }
    }


    void Update()
    {
        transform.LookAt(target.position);
        transform.Rotate(new Vector3(0, -90, 0), Space.Self);

        if (keepGround)
        {

            if (Vector3.Distance(transform.position, target.position) < followDistance)
            {
                if (!stopAtMinimumDistance)
                {
                    Vector3 distFromPlayer = target.position - transform.position;
                    Vector3 velocity = distFromPlayer.normalized * speed;
                    velocity.y = -30; // Apply gravity

                    rb.velocity = velocity;
                }
                else if (stopAtMinimumDistance && Vector3.Distance(transform.position, target.position) > mininumDistance)
                {
                    Vector3 distFromPlayer = target.position - transform.position;
                    Vector3 velocity = distFromPlayer.normalized * speed;
                    velocity.y = -30; // Apply gravity

                    rb.velocity = velocity;
                }
            }


        }

        if (Vector3.Distance(transform.position, target.position) < followDistance)
        {
            if (!stopAtMinimumDistance)
            {
                transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
            } else if (stopAtMinimumDistance && Vector3.Distance(transform.position, target.position) > mininumDistance)
            {
                transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
            }
        }
    }

}
