﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	public GameObject target;
	public float lerpSpeed = 5f;

	private Vector3 cameraPos;

	// Use this for initialization
	void Start () {
		cameraPos.z = transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
		//cameraPos = new Vector3(target.transform.position.x, target.transform.position.y, transform.position.z);
		cameraPos.x = Mathf.Lerp (transform.position.x, target.transform.position.x, Time.deltaTime * lerpSpeed);
		cameraPos.y = Mathf.Lerp (transform.position.y, target.transform.position.y, Time.deltaTime * lerpSpeed);

		transform.position = cameraPos;
	}
}
